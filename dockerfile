FROM ubuntu:16.04

ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid

COPY startup.sh .
RUN chmod 755 startup.sh

WORKDIR /var/www/html/

RUN apt update
RUN apt-get -y install  apache2 curl net-tools git php php-mysql php-gd php-curl php-json php-cgi php-fpm php-mbstring php-xml php-zip libapache2-mod-php redis-server composer
RUN a2enmod rewrite
RUN a2enmod php7.0
RUN echo "Listen 5000" >> /etc/apache2/ports.conf
RUN git clone https://github.com/laravel/laravel
RUN cd laravel && git checkout 5.5 && chmod 777 -R . && cd ..

COPY 000-default.conf /etc/apache2/sites-enabled/000-default.conf

WORKDIR /var/www/html/laravel
RUN curl -sS https://getcomposer.org/installer | php && chmod 755 composer.phar
RUN mv composer.phar composer
RUN ./composer install

WORKDIR /
CMD ./startup.sh
